# LEMP Stack Docker Image

[![](https://images.microbadger.com/badges/image/adipriyantobpn/phusion-lemp.svg)](https://microbadger.com/images/adipriyantobpn/phusion-lemp "Get your own image badge on microbadger.com")

## Description

Instead of using separate containers to provide different service, this Docker image contains Nginx, MySQL, and PHP in single container.

Based on phusion/baseimage which encourage multiple process running in single container.

## Accessing Containers

### Direct Testing

To test this container, just execute this command below. Environment variables also can be passed into it on the fly.

```shell
$ docker run --rm -it -P adipriyantobpn/lemp-phusion /sbin/my_init -- bash -l
```

### Using SSH

Instead of passing instruction into container using `docker exec`, logging in using SSH will work better. Don't forget to include your SSH key when first time running this image.

```shell
$ docker run -d --name lempstack -p 80:80 -p 6022:22 -v $(pwd)/ssh.pub:/root/.ssh/authorized_keys adipriyantobpn/lemp-phusion

$ ssh -i $(pwd)/id -p 6022 root@$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' lempstack)
```

## Applications
- Phusion Base Image 0.9.19
- Runit 2.1.2
- Nginx 1.10.0
- MySQL 5.7.15
- PHP-FPM 7.0.8
- OpenSSH 7.2

## Supported Environment Variables

> Note : prepared using Excel, then generated to Markdown table using https://ozh.github.io/ascii-tables/

| Application |         Variable         |   Valid Input    | Default Value | Description                                                                                                                                                                                                                                                                                                                                             |
|-------------|--------------------------|------------------|---------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Nginx       | DOCROOT                  | Directory path   | /var/www/html | Configure the document root path                                                                                                                                                                                                                                                                                                                        |
|             | FRONT_CONTROLLER         | File name        | index.php     | Index file for web application, eg. app.php, app_dev.php                                                                                                                                                                                                                                                                                                |
|             | SYNC_UID                 | 1                | null          | When using volumes on the host system, the UIDs may not match up with the UIDs on the container and may create permission issues.<br/>SYNC_UID=1 will instruct the container to look at the UID and GID of the DOCROOT and update the www-data UID and GID to match.<br/>In production, the recommended solution is to use volume containers.           |
| PHP-FPM     | TZ                       | Timezone value   | UTC           | Configure the timezone in php.ini                                                                                                                                                                                                                                                                                                                       |
|             | DEBUG                    | 1                | null          | Enable xdebug module, enable display_errors, set memory_limit to 2 GB                                                                                                                                                                                                                                                                                   |
|             | PHP_MEMORY_LIMIT         | Memory size unit | 128M          | Set memory_limit                                                                                                                                                                                                                                                                                                                                        |
| MySQL       | SYNC_UID                 | 1                | null          | Same as SYNC_UID for Nginx                                                                                                                                                                                                                                                                                                                              |
|             | MYSQL_OPTS               | MySQL Options    | null          | Options that will be passed to mysqld_safe for first time initializing database                                                                                                                                                                                                                                                                         |
|             | ADMIN_USER               | Username         | admin         | MySQL username that will be automatically generated                                                                                                                                                                                                                                                                                                     |
|             | ADMIN_PASS               | Password         | null          | Password for ADMIN_USER                                                                                                                                                                                                                                                                                                                                 |
|             | ADMIN_HOST               | IP address / %   | %             | Hostname that can access MySQL using ADMIN_USER                                                                                                                                                                                                                                                                                                         |
|             | MYSQL_MAX_ALLOWED_PACKET | Memory size unit | 16M           | Configure upper limit on the size of any single message between the MySQL server and clients, including replication slaves                                                                                                                                                                                                                              |
| System      | UBUNTU_REPO_INDONESIA    | 1                | null          | Specify whether Apt will use Ubuntu Repository in Indonesia (http://kambing.ui.ac.id/)                                                                                                                                                                                                                                                                  |

## Installed Application Detail

- adduser/now 3.113+nmu3ubuntu4 all
- apparmor/now 2.10.95-0ubuntu2.2 amd64
- apt/now 1.2.12~ubuntu16.04.1 amd64
- apt-transport-https/now 1.2.12~ubuntu16.04.1 amd64
- base-files/now 9.4ubuntu4.1 amd64
- base-passwd/now 3.5.39 amd64
- bash/now 4.3-14ubuntu1.1 amd64
- bsdutils/now 1:2.27.1-6ubuntu3.1 amd64
- busybox-initramfs/now 1:1.22.0-15ubuntu1 amd64
- ca-certificates/now 20160104ubuntu1 all
- coreutils/now 8.25-2ubuntu2 amd64
- cpio/now 2.11+dfsg-5ubuntu1 amd64
- cron/now 3.0pl1-128ubuntu2 amd64
- curl/now 7.47.0-1ubuntu2 amd64
- dash/now 0.5.8-2.1ubuntu2 amd64
- debconf/now 1.5.58ubuntu1 all
- debianutils/now 4.7 amd64
- dh-python/now 2.20151103ubuntu1.1 all
- diffutils/now 1:3.3-3 amd64
- distro-info-data/now 0.28ubuntu0.1 all
- dpkg/now 1.18.4ubuntu1.1 amd64
- e2fslibs/now 1.42.13-1ubuntu1 amd64
- e2fsprogs/now 1.42.13-1ubuntu1 amd64
- file/now 1:5.25-2ubuntu1 amd64
- findutils/now 4.6.0+git+20160126-2 amd64
- fontconfig-config/now 2.11.94-0ubuntu1.1 all
- fonts-dejavu-core/now 2.35-1 all
- gcc-5-base/now 5.3.1-14ubuntu2.1 amd64
- gcc-6-base/now 6.0.1-0ubuntu1 amd64
- gir1.2-glib-2.0/now 1.46.0-3ubuntu1 amd64
- gnupg/now 1.4.20-1ubuntu3 amd64
- gpgv/now 1.4.20-1ubuntu3 amd64
- grep/now 2.25-1~16.04.1 amd64
- gzip/now 1.6-4ubuntu1 amd64
- hostname/now 3.16ubuntu2 amd64
- init/now 1.29ubuntu2 amd64
- init-system-helpers/now 1.29ubuntu2 all
- initramfs-tools/now 0.122ubuntu8.1 all
- initramfs-tools-bin/now 0.122ubuntu8.1 amd64
- initramfs-tools-core/now 0.122ubuntu8.1 all
- initscripts/now 2.88dsf-59.3ubuntu2 amd64
- insserv/now 1.14.0-5ubuntu3 amd64
- iso-codes/now 3.65-1 all
- klibc-utils/now 2.0.4-8ubuntu1.16.04.1 amd64
- kmod/now 22-1ubuntu4 amd64
- language-pack-en/now 1:16.04+20160415 all
- language-pack-en-base/now 1:16.04+20160415 all
- less/now 481-2.1 amd64
- libacl1/now 2.2.52-3 amd64
- libaio1/now 0.3.110-2 amd64
- libapparmor-perl/now 2.10.95-0ubuntu2.2 amd64
- libapparmor1/now 2.10.95-0ubuntu2 amd64
- libapt-inst2.0/now 1.2.12~ubuntu16.04.1 amd64
- libapt-pkg5.0/now 1.2.12~ubuntu16.04.1 amd64
- libasn1-8-heimdal/now 1.7~git20150920+dfsg-4ubuntu1 amd64
- libattr1/now 1:2.4.47-2 amd64
- libaudit-common/now 1:2.4.5-1ubuntu2 all
- libaudit1/now 1:2.4.5-1ubuntu2 amd64
- libblkid1/now 2.27.1-6ubuntu3.1 amd64
- libbsd0/now 0.8.2-1 amd64
- libbz2-1.0/now 1.0.6-8 amd64
- libc-bin/now 2.23-0ubuntu3 amd64
- libc6/now 2.23-0ubuntu3 amd64
- libcap2/now 1:2.24-12 amd64
- libcap2-bin/now 1:2.24-12 amd64
- libcomerr2/now 1.42.13-1ubuntu1 amd64
- libcryptsetup4/now 2:1.6.6-5ubuntu2 amd64
- libcurl3/now 7.47.0-1ubuntu2.1 amd64
- libcurl3-gnutls/now 7.47.0-1ubuntu2 amd64
- libdb5.3/now 5.3.28-11 amd64
- libdbus-1-3/now 1.10.6-1ubuntu3 amd64
- libdbus-glib-1-2/now 0.106-1 amd64
- libdebconfclient0/now 0.198ubuntu1 amd64
- libdevmapper1.02.1/now 2:1.02.110-1ubuntu10 amd64
- libedit2/now 3.1-20150325-1ubuntu2 amd64
- libevent-core-2.0-5/now 2.0.21-stable-2 amd64
- libevtlog0/now 0.2.12-7 amd64
- libexpat1/now 2.1.0-7ubuntu0.16.04.2 amd64
- libfdisk1/now 2.27.1-6ubuntu3.1 amd64
- libffi6/now 3.2.1-4 amd64
- libfontconfig1/now 2.11.94-0ubuntu1.1 amd64
- libfreetype6/now 2.6.1-0.1ubuntu2 amd64
- libgcc1/now 1:6.0.1-0ubuntu1 amd64
- libgcrypt20/now 1.6.5-2 amd64
- libgd3/now 2.1.1-4ubuntu0.16.04.3 amd64
- libgdbm3/now 1.8.3-13.1 amd64
- libgeoip1/now 1.6.9-1 amd64
- libgirepository-1.0-1/now 1.46.0-3ubuntu1 amd64
- libglib2.0-0/now 2.48.1-1~ubuntu16.04.1 amd64
- libgmp10/now 2:6.1.0+dfsg-2 amd64
- libgnutls30/now 3.4.10-4ubuntu1.1 amd64
- libgpg-error0/now 1.21-2ubuntu1 amd64
- libgssapi-krb5-2/now 1.13.2+dfsg-5 amd64
- libgssapi3-heimdal/now 1.7~git20150920+dfsg-4ubuntu1 amd64
- libhcrypto4-heimdal/now 1.7~git20150920+dfsg-4ubuntu1 amd64
- libheimbase1-heimdal/now 1.7~git20150920+dfsg-4ubuntu1 amd64
- libheimntlm0-heimdal/now 1.7~git20150920+dfsg-4ubuntu1 amd64
- libhogweed4/now 3.2-1 amd64
- libhx509-5-heimdal/now 1.7~git20150920+dfsg-4ubuntu1 amd64
- libicu55/now 55.1-7 amd64
- libidn11/now 1.32-3ubuntu1 amd64
- libivykis0/now 0.36.2-1 amd64
- libjbig0/now 2.1-3.1 amd64
- libjpeg-turbo8/now 1.4.2-0ubuntu3 amd64
- libjpeg8/now 8c-2ubuntu8 amd64
- libk5crypto3/now 1.13.2+dfsg-5 amd64
- libkeyutils1/now 1.5.9-8ubuntu1 amd64
- libklibc/now 2.0.4-8ubuntu1.16.04.1 amd64
- libkmod2/now 22-1ubuntu4 amd64
- libkrb5-26-heimdal/now 1.7~git20150920+dfsg-4ubuntu1 amd64
- libkrb5-3/now 1.13.2+dfsg-5 amd64
- libkrb5support0/now 1.13.2+dfsg-5 amd64
- libldap-2.4-2/now 2.4.42+dfsg-2ubuntu3.1 amd64
- liblz4-1/now 0.0~r131-2ubuntu2 amd64
- liblzma5/now 5.1.1alpha+20120614-2ubuntu2 amd64
- libmagic1/now 1:5.25-2ubuntu1 amd64
- libmcrypt4/now 2.5.8-3.3 amd64
- libmount1/now 2.27.1-6ubuntu3.1 amd64
- libmpdec2/now 2.4.2-1 amd64
- libncurses5/now 6.0+20160213-1ubuntu1 amd64
- libncursesw5/now 6.0+20160213-1ubuntu1 amd64
- libnet1/now 1.1.6+dfsg-3 amd64
- libnettle6/now 3.2-1 amd64
- libp11-kit0/now 0.23.2-3 amd64
- libpam-doc/now 1.1.8-3.2ubuntu2 all
- libpam-modules/now 1.1.8-3.2ubuntu2 amd64
- libpam-modules-bin/now 1.1.8-3.2ubuntu2 amd64
- libpam-runtime/now 1.1.8-3.2ubuntu2 all
- libpam0g/now 1.1.8-3.2ubuntu2 amd64
- libpcre3/now 2:8.38-3.1 amd64
- libperl5.22/now 5.22.1-9 amd64
- libpng12-0/now 1.2.54-1ubuntu1 amd64
- libpopt0/now 1.16-10 amd64
- libprocps4/now 2:3.3.10-4ubuntu2 amd64
- libpython3-stdlib/now 3.5.1-3 amd64
- libpython3.5-minimal/now 3.5.1-10 amd64
- libpython3.5-stdlib/now 3.5.1-10 amd64
- libreadline6/now 6.3-8ubuntu2 amd64
- libroken18-heimdal/now 1.7~git20150920+dfsg-4ubuntu1 amd64
- librtmp1/now 2.4+20151223.gitfa8646d-1build1 amd64
- libsasl2-2/now 2.1.26.dfsg1-14build1 amd64
- libsasl2-modules-db/now 2.1.26.dfsg1-14build1 amd64
- libseccomp2/now 2.2.3-3ubuntu3 amd64
- libselinux1/now 2.4-3build2 amd64
- libsemanage-common/now 2.3-1build3 all
- libsemanage1/now 2.3-1build3 amd64
- libsepol1/now 2.4-2 amd64
- libsmartcols1/now 2.27.1-6ubuntu3.1 amd64
- libsqlite3-0/now 3.11.0-1ubuntu1 amd64
- libss2/now 1.42.13-1ubuntu1 amd64
- libssl1.0.0/now 1.0.2g-1ubuntu4.1 amd64
- libstdc++6/now 5.3.1-14ubuntu2.1 amd64
- libsystemd0/now 229-4ubuntu6 amd64
- libtasn1-6/now 4.7-3ubuntu0.16.04.1 amd64
- libtiff5/now 4.0.6-1 amd64
- libtinfo5/now 6.0+20160213-1ubuntu1 amd64
- libudev1/now 229-4ubuntu8 amd64
- libusb-0.1-4/now 2:0.1.12-28 amd64
- libustr-1.0-1/now 1.0.4-5 amd64
- libuuid1/now 2.27.1-6ubuntu3.1 amd64
- libvpx3/now 1.5.0-2ubuntu1 amd64
- libwind0-heimdal/now 1.7~git20150920+dfsg-4ubuntu1 amd64
- libwrap0/now 7.6.q-25 amd64
- libx11-6/now 2:1.6.3-1ubuntu2 amd64
- libx11-data/now 2:1.6.3-1ubuntu2 all
- libxau6/now 1:1.0.8-1 amd64
- libxcb1/now 1.11.1-1ubuntu1 amd64
- libxdmcp6/now 1:1.1.2-1.1 amd64
- libxml2/now 2.9.3+dfsg1-1ubuntu0.1 amd64
- libxpm4/now 1:3.5.11-1 amd64
- libxslt1.1/now 1.1.28-2.1 amd64
- libzip4/now 1.0.1-0ubuntu1 amd64
- linux-base/now 4.0ubuntu1 all
- locales/now 2.23-0ubuntu3 all
- login/now 1:4.2-3.1ubuntu5 amd64
- logrotate/now 3.8.7-2ubuntu2 amd64
- lsb-base/now 9.20160110ubuntu0.2 all
- lsb-release/now 9.20160110ubuntu0.2 all
- makedev/now 2.3.1-93ubuntu1 all
- mawk/now 1.3.3-17ubuntu2 amd64
- mime-support/now 3.59ubuntu1 all
- mount/now 2.27.1-6ubuntu3.1 amd64
- multiarch-support/now 2.23-0ubuntu3 amd64
- mysql-client-5.7/now 5.7.15-0ubuntu0.16.04.1 amd64
- mysql-client-core-5.7/now 5.7.15-0ubuntu0.16.04.1 amd64
- mysql-common/now 5.7.15-0ubuntu0.16.04.1 all
- mysql-server/now 5.7.15-0ubuntu0.16.04.1 all
- mysql-server-5.7/now 5.7.15-0ubuntu0.16.04.1 amd64
- mysql-server-core-5.7/now 5.7.15-0ubuntu0.16.04.1 amd64
- ncurses-base/now 6.0+20160213-1ubuntu1 all
- ncurses-bin/now 6.0+20160213-1ubuntu1 amd64
- nginx/now 1.10.0-0ubuntu0.16.04.2 all
- nginx-common/now 1.10.0-0ubuntu0.16.04.2 all
- nginx-core/now 1.10.0-0ubuntu0.16.04.2 amd64
- openssh-client/now 1:7.2p2-4ubuntu1 amd64
- openssh-server/now 1:7.2p2-4ubuntu1 amd64
- openssh-sftp-server/now 1:7.2p2-4ubuntu1 amd64
- openssl/now 1.0.2g-1ubuntu4.1 amd64
- passwd/now 1:4.2-3.1ubuntu5 amd64
- perl/now 5.22.1-9 amd64
- perl-base/now 5.22.1-9 amd64
- perl-modules-5.22/now 5.22.1-9 all
- php/now 1:7.0+35ubuntu6 all
- php-bcmath/now 1:7.0+35ubuntu6 all
- php-common/now 1:35ubuntu6 all
- php-curl/now 1:7.0+35ubuntu6 all
- php-gd/now 1:7.0+35ubuntu6 all
- php-intl/now 1:7.0+35ubuntu6 all
- php-mbstring/now 1:7.0+35ubuntu6 all
- php-mcrypt/now 1:7.0+35ubuntu6 all
- php-mysql/now 1:7.0+35ubuntu6 all
- php-xdebug/now 2.4.0-1 amd64
- php-zip/now 1:7.0+35ubuntu6 all
- php7.0/now 7.0.8-0ubuntu0.16.04.2 all
- php7.0-bcmath/now 7.0.8-0ubuntu0.16.04.2 amd64
- php7.0-cli/now 7.0.8-0ubuntu0.16.04.2 amd64
- php7.0-common/now 7.0.8-0ubuntu0.16.04.2 amd64
- php7.0-curl/now 7.0.8-0ubuntu0.16.04.2 amd64
- php7.0-fpm/now 7.0.8-0ubuntu0.16.04.2 amd64
- php7.0-gd/now 7.0.8-0ubuntu0.16.04.2 amd64
- php7.0-intl/now 7.0.8-0ubuntu0.16.04.2 amd64
- php7.0-json/now 7.0.8-0ubuntu0.16.04.2 amd64
- php7.0-mbstring/now 7.0.8-0ubuntu0.16.04.2 amd64
- php7.0-mcrypt/now 7.0.8-0ubuntu0.16.04.2 amd64
- php7.0-mysql/now 7.0.8-0ubuntu0.16.04.2 amd64
- php7.0-opcache/now 7.0.8-0ubuntu0.16.04.2 amd64
- php7.0-readline/now 7.0.8-0ubuntu0.16.04.2 amd64
- php7.0-xml/now 7.0.8-0ubuntu0.16.04.2 amd64
- php7.0-zip/now 7.0.8-0ubuntu0.16.04.2 amd64
- procps/now 2:3.3.10-4ubuntu2 amd64
- psmisc/now 22.21-2.1build1 amd64
- pwgen/now 2.07-1.1ubuntu1 amd64
- python-apt-common/now 1.1.0~beta1build1 all
- python3/now 3.5.1-3 amd64
- python3-apt/now 1.1.0~beta1build1 amd64
- python3-dbus/now 1.2.0-3 amd64
- python3-gi/now 3.20.0-0ubuntu1 amd64
- python3-minimal/now 3.5.1-3 amd64
- python3-pycurl/now 7.43.0-1ubuntu1 amd64
- python3-software-properties/now 0.96.20.2 all
- python3.5/now 3.5.1-10 amd64
- python3.5-minimal/now 3.5.1-10 amd64
- readline-common/now 6.3-8ubuntu2 all
- runit/now 2.1.2-3ubuntu1 amd64
- sed/now 4.2.2-7 amd64
- sensible-utils/now 0.0.9 all
- software-properties-common/now 0.96.20.2 all
- syslog-ng-core/now 3.5.6-2.1 amd64
- systemd/now 229-4ubuntu6 amd64
- systemd-sysv/now 229-4ubuntu6 amd64
- sysv-rc/now 2.88dsf-59.3ubuntu2 all
- sysvinit-utils/now 2.88dsf-59.3ubuntu2 amd64
- tar/now 1.28-2.1 amd64
- tzdata/now 2016f-0ubuntu0.16.04 all
- ubuntu-keyring/now 2012.05.19 all
- ucf/now 3.0036 all
- udev/now 229-4ubuntu8 amd64
- util-linux/now 2.27.1-6ubuntu3.1 amd64
- vim-common/now 2:7.4.1689-3ubuntu1.1 amd64
- vim-tiny/now 2:7.4.1689-3ubuntu1.1 amd64
- zlib1g/now 1:1.2.8.dfsg-2ubuntu4 amd64
