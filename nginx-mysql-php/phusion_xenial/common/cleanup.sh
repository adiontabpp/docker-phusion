#!/bin/sh

# Delete all .deb files
apt-get clean

rm -f /etc/dpkg/dpkg.cfg.d/02apt-speedup

# Delete all APT cache files
rm -rf /var/lib/apt/lists/*

# Delete temporary files
rm -rf /tmp/*
rm -rf /var/tmp/*
