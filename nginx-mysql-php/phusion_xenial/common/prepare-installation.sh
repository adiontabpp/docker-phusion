#!/bin/bash

# Set dpkg to not instruct kernel to sync stuff to disk quite often when unpacking packages
# WARNING: Using this option might improve performance at the cost of losing data, use with care.
#   https://unburden-home-dir.readthedocs.io/en/latest/see-also/#io-during-upgrading-packages
echo force-unsafe-io > /etc/dpkg/dpkg.cfg.d/02apt-speedup

# Use Indonesian repository
if [ -n "$UBUNTU_REPO_INDONESIA" ]
then
    cp /etc/apt/sources.list /etc/apt/sources.list.bak
    sed -i 's/archive.ubuntu.com/kambing.ui.ac.id/g' /etc/apt/sources.list
fi

apt-get update
