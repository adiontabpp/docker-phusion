#!/bin/bash

# Use the TZ environment variable, otherwise use UTC
PHP_TIMEZONE="UTC"
if [ -n "${TZ}" ]
then
    PHP_TIMEZONE="$TZ"
fi

find /etc/php -name php.ini -print0 | xargs -0 sed -i "s#;date.timezone =.*#date.timezone = $PHP_TIMEZONE#"

# Configuration for debugging
if [ ! -z "$DEBUG" ]
then
    phpenmod xdebug
    sed -i 's#^display_errors = Off#display_errors = On#' /etc/php/7.0/fpm/php.ini
    sed -i -r "s/memory_limit.+$/memory_limit = 2G/g" /etc/php/7.0/fpm/php.ini
fi

# Set memory limit
#   Recommendation :
#   - Compiling code, 768M
#   - Deploying static asses, 768M
#   - Testing, 2G
#   - Installing and updating Magento components from Magento Marketplace, 2G
if [ -n "$PHP_MEMORY_LIMIT" ]
then
    sed -i -r "s/memory_limit.+$/memory_limit = $PHP_MEMORY_LIMIT/g" /etc/php/7.0/fpm/php.ini
fi

# Add environment variables to the php-fpm configuration
# Based on startup.sh from https://github.com/dubture-dockerfiles/nginx-php
set_phpfpm_env_var() {
    if [ -z "$2" ]
    then
            echo "Environment variable '$1' not set."
            return
    fi

    # Check whether variable already exists
    if grep "^env\[$1\]=" /etc/php/7.0/fpm/pool.d/www.conf
    then
        sed -i "s#^env\[$1\]=.*#env[$1]=\"$2\"#g" /etc/php/7.0/fpm/pool.d/www.conf
    else
        echo "env[$1]=\"$2\"" >> /etc/php/7.0/fpm/pool.d/www.conf
    fi
}

# Grep for variables that look like docker set them (_PORT_)
for file in /etc/container_environment/*
do
    var=$(basename $file)
    val=$(cat $file)
    echo $var $val
    set_phpfpm_env_var $var $val
done
