#!/bin/bash

# Run PHP-FPM as non-daemon mode
sed -i -e "s/;daemonize\s*=\s*yes/daemonize = no/g" /etc/php/7.0/fpm/php-fpm.conf
sed -i "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/" /etc/php/7.0/fpm/php.ini

# Move .pid and .lock files into /run dir
sed -i 's#pid = /run/php/php7.0-fpm.pid#pid = /run/php7.0-fpm.pid#' /etc/php/7.0/fpm/php-fpm.conf
sed -i 's#listen = /run/php/php7.0-fpm.sock#listen = /run/php7.0-fpm.sock#' /etc/php/7.0/fpm/pool.d/www.conf

# Configure PHP (timezone, debug, memory_limit) on startup
cp -a /build/php-fpm/init.sh /etc/my_init.d/00_configure_php.sh
chmod +x /etc/my_init.d/00_configure_php.sh

# Configure PHP-FPM to start as a service
if [ -d /etc/service/php-fpm ]; then
    rm -rf /etc/service/php-fpm
fi
mkdir /etc/service/php-fpm
cp -a /build/php-fpm/runit.sh /etc/service/php-fpm/run
chmod +x /etc/service/php-fpm/run
phpenmod mcrypt

# Ensure the mode is correct on the unix socket
sed -i 's#;listen.mode = 0660#listen.mode = 0666#g' /etc/php/7.0/fpm/pool.d/www.conf

# Disable xdebug by default
phpdismod xdebug
