#!/bin/bash

# Listen on all interfaces
sed -i "s#^bind-address\s*=\s*.*#bind-address = 0.0.0.0#" /etc/mysql/mysql.conf.d/mysqld.cnf

# key_buffer_size is deprecated
sed -i 's#key_buffer_size =#key_buffer =#g' /etc/mysql/mysql.conf.d/mysqld.cnf

# Set max allowed packet
if [ -n "$MYSQL_MAX_ALLOWED_PACKET" ]
then
    sed -i -r "s/max_allowed_packet.+$/max_allowed_packet = $MYSQL_MAX_ALLOWED_PACKET/g" /etc/mysql/mysql.conf.d/mysqld.cnf
    sed -i -r "s/max_allowed_packet.+$/max_allowed_packet = $MYSQL_MAX_ALLOWED_PACKET/g" /etc/mysql/conf.d/mysqldump.cnf
fi

# Configure mysql to start as a service
if [ -d /etc/service/mysql ]; then
    rm -rf /etc/service/mysql
fi
mkdir /etc/service/mysql
cp -a /build/mysql/runit.sh /etc/service/mysql/run
chmod +x /etc/service/mysql/run
