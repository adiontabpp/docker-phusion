#!/bin/bash

# Run Nginx as non-daemon mode
if grep "^daemon off;" /etc/nginx/nginx.conf
then
    echo "Nginx is already set to not daemonized"
else
    echo "daemon off;" >> /etc/nginx/nginx.conf
fi

# Hide Nginx version token in HTTP header
sed -i -e "s/# server_tokens off;/server_tokens off;/g" /etc/nginx/nginx.conf

# Prepare default site config
cp -a /build/nginx/site-default /etc/nginx/sites-available/default

# Configure Nginx (DOCROOT and FRONT_CONTROLLER) on startup in default site
cp -a /build/nginx/init.sh /etc/my_init.d/00_configure_nginx.sh
chmod +x /etc/my_init.d/00_configure_nginx.sh

# Configure Nginx to start as a service
if [ -d /etc/service/nginx ]; then
    rm -rf /etc/service/nginx
fi
mkdir /etc/service/nginx
cp -a /build/nginx/runit.sh /etc/service/nginx/run
chmod +x /etc/service/nginx/run
