#!/bin/bash

# Use the DOCROOT environment variable, otherwise use /var/www
NGINX_DOCROOT="/var/www/html"
if [ -n "${DOCROOT}" ]
then
    NGINX_DOCROOT="$DOCROOT"
fi

sed -i "s#root .*;#root $NGINX_DOCROOT;#" /etc/nginx/sites-enabled/default
mkdir -p $NGINX_DOCROOT

if [ -n "${FRONT_CONTROLLER}" ]
then
    sed -i "s#try_files \$uri \$uri/ /index.php\$is_args\$args;#try_files \$uri \$uri/ /${FRONT_CONTROLLER}\$is_args\$args;#" /etc/nginx/sites-enabled/default
fi

if [ -n "$SYNC_UID" -a "$SYNC_UID" = "1" ]
then
    uid=`stat -c '%u' $DOCROOT`
    gid=`stat -c '%g' $DOCROOT`

    echo "Updating www-data ids to ($uid:$gid)"

    if [ ! $uid -eq 0 ]
    then
        sed -i "s#^www-data:x:.*:.*:#www-data:x:$uid:$gid:#" /etc/passwd
    fi

    if [ ! $gid -eq 0 ]
    then
        sed -i "s#^www-data:x:.*:#www-data:x:$gid:#" /etc/group
    fi

    chown www-data /var/log/nginx
fi
